<?php 
	get_header(); 
	//Template name: Home
?>

<div class="container">
  <div class="row">
    <div class="form-consulta d-block d-sm-none p-3">
      <?php $titulo_formulario= get_field('titulo_formulario'); ?>
      <h3 class="title-contato"><?php echo $titulo_formulario; ?></h3>
      <?php echo do_shortcode('[contact-form-7 id="258" title="Contato"]'); ?>
    </div>
  </div>
</div>

  <section class="description">
    <div class="container">
      <?php $titulo_box_pos_topo = get_field('titulo_box_pos_topo'); ?>
      <h2 class="title-principal"><?php echo $titulo_box_pos_topo; ?></h2>
      <?php $campo_box_pos_topo = get_field('campo_box_pos_topo'); ?>
      <p class="text"><?php echo $campo_box_pos_topo; ?></p>
    </div>
  </section>

  <section class="especialidades">
    <?php $titulo_especialidades = get_field('titulo_box_especialidades'); ?>
    <h2 class="title-principal col-12 d-block d-sm-none"><?php echo $titulo_especialidades; ?></h2>
    <?php $imagem_box_especialidades = get_field('imagem_box_especialidades'); ?>
    <div class="bg-especialidades-mobile d-block d-sm-none col-12 col-sm-6 col-md-4" style="background: url('<?php echo $imagem_box_especialidades['url'];?>')">
    </div>
    <div class="container">
      <div class="row">
        <div class="bg-especialidades d-none d-sm-block col-12 col-sm-6 col-md-4" style="background: url('<?php echo $imagem_box_especialidades['url'];?>')">
        </div>

        <div class="content col-12 col-sm-6 col-md-8 text-center">
          <h2 class="title-principal d-none d-sm-block"><?php echo $titulo_especialidades; ?></h2>
          <ul class="list-unstyled">
            <?php
              $especialidades = get_field('especialidades');
              for( $i=0; $i < count($especialidades); $i++) {
                $especialidade = $especialidades[$i]['especialidade'];
            ?>
                <li><i class="mdi mdi-check"></i><?php echo $especialidade; ?></li>
            <?php
              }
            ?>
            
          </ul>
        </div>
      </div>
    </div>
  </section>
  <div class="clearfix"></div>
  
  <div class="container">
    <div class="row">
      <div class="informativo col-sm-6 col-md-4 d-none d-sm-block">
        <span>Mais de 40 Unidades por todo o nordeste!</span>
        <span>Encontre a <b>OrtoEstética</b> mais próxima de você</span>
      </div>
    </div>
  </div>


  <section class="mapa">
    <div class="informativo col-12 d-block d-sm-none">
      <span>Mais de 40 Unidades por todo o nordeste!</span>
      <span>Encontre a <b>OrtoEstética</b> mais próxima de você</span>
    </div>
    <iframe src="https://snazzymaps.com/embed/90266" width="100%" height="400px" style="border:none;"></iframe>
  </section>


<?php get_footer(); ?>
