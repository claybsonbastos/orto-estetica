<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <title><?php wp_title( '' ); ?><?php if ( wp_title( '', false ) ) {
		  echo ' :';
	  } ?><?php bloginfo( 'name' ); ?></title>

  <link href="//www.google-analytics.com" rel="dns-prefetch">
  <link href="<?php echo get_template_directory_uri(); ?>/img/favicon.png" rel="shortcut icon">
  <link href="<?php echo get_template_directory_uri(); ?>/img/favicon.png" rel="apple-touch-icon-precomposed">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="<?php bloginfo( 'description' ); ?>">

	<?php wp_head(); ?>
  <script>
    // conditionizr.com
    // configure environment tests
    conditionizr.config({
      assets: '<?php echo get_template_directory_uri(); ?>',
      tests: {}
    });
  </script>

</head>
<body <?php body_class(); ?>>
<!-- wrapper -->
<div class="container-fluid">
  <!-- header -->
  <header class="header">
    <!-- logo -->
    <div class="text-center col-10 offset-1 col-sm-12 offset-sm-0">
      <?php $logo_topo = get_field('logo_topo'); ?>
      <a href="<?php echo home_url(); ?>">
        <img class="logo-img" src="<?php echo $logo_topo['url']; ?>" alt="Logo">
      </a>
    </div>
    <!-- /logo -->
    
    <!-- phone -->
    <div class="phone d-none d-md-block">
      <span><i class="mdi mdi-phone-in-talk"></i>Ligue Agora:<a href="tel:00000000000">(--)----- ----</a></span>
    </div>
    <!-- /phone -->
    <div class="clearfix"></div>
    <div class="banner-topo">
      <div class="container">
        <div class="row">
          <?php $titulo_topo = get_field('titulo_topo'); ?>
          <h2 class=" col-12 offset-sm-5 col-sm-7 title-topo"><?php echo $titulo_topo; ?></h2>

          <div class="form-consulta offset-sm-5 col-sm-7 col-md-6 d-none d-sm-block">
            <?php $titulo_formulario = get_field('titulo_formulario'); ?>
            <h3 class="title-contato"><?php echo $titulo_formulario; ?></h3>
            <?php echo do_shortcode('[contact-form-7 id="258" title="Contato"]'); ?>
          </div>

        </div>
      </div>
    </div>
  </header>
  <!-- /header -->
