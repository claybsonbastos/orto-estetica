<!-- footer -->
<footer class="footer">
  <div class="container">
    <div class="row">
      <div class="logo-footer col-12 col-sm-4">
        <?php $logo_rodape = get_field('logo_rodape'); ?>
        <a href="#"><img src="<?php echo $logo_rodape['url'];?>" alt=""></a>
      </div>

      <div class="copyright col-12 col-sm-4 text-center">
        <?php $campo_rodape = get_field('campo_rodape'); ?>
        <span><?php echo $campo_rodape; ?></span>
      </div>

      <div class="social col-12 col-sm-4">
        <?php 
          $link_do_facebook = get_field('link_do_facebook');
          $link_do_instagram = get_field('link_do_instagram');
        ?>
        <a href="<?php echo $link_do_instagram; ?>"><i class="mdi mdi-instagram"></i></a>
        <a href="<?php echo $link_do_facebook; ?>"><i class="mdi mdi-facebook"></i></a>
      </div>
    </div>
  </div>

  <div class="phone-footer d-block d-sm-none">
    <span><i class="mdi mdi-phone-in-talk"></i>Ligue Agora:<a href="tel:00000000000">(--)----- ----</a></span>
  </div>
</footer>
<!-- /footer -->
</div>
<!-- /wrapper -->
<!-- JAVASCRIPT -->
<?php
// wp_enqueue_script( 'slick', get_bloginfo( 'template_directory' ) . '/libs/slick-carousel/slick/slick.min.js', array( 'jquery' ), '', true );
// wp_enqueue_script( 'fancybox', get_bloginfo( 'template_directory' ) . '/libs/fancybox/dist/jquery.fancybox.min.js', array(), '', true );
wp_enqueue_script( 'jQuery', get_bloginfo( 'template_directory' ) . '/libs/jquery/dist/jquery.min.js', array( 'jquery' ), '', true );
wp_enqueue_script( 'bootstrap', get_bloginfo( 'template_directory' ) . '/libs/bootstrap/dist/js/bootstrap.min.js', array( 'jquery' ), '', true );
wp_enqueue_script( 'main', get_bloginfo( 'template_directory' ) . '/js/main.js', array( 'jquery' ), '', true );
?>

<?php wp_footer(); ?>

<!-- analytics -->
<script>
  (function (f, i, r, e, s, h, l) {
    i['GoogleAnalyticsObject'] = s;
    f[s] = f[s] || function () {
        (f[s].q = f[s].q || []).push(arguments)
      }, f[s].l = 1 * new Date();
    h = i.createElement(r),
      l = i.getElementsByTagName(r)[0];
    h.async = 1;
    h.src = e;
    l.parentNode.insertBefore(h, l)
  })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
  ga('create', 'UA-XXXXXXXX-XX', 'yourdomain.com');
  ga('send', 'pageview');
</script>

</body>
</html>
