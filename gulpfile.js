var gulp = require('gulp');
var sass = require('gulp-sass');
var pump = require('pump');
var browserSync = require('browser-sync');
var uglify = require('gulp-uglify');
var runSequence = require('run-sequence');

// Compila o SASS
gulp.task('sass', function(){
  return gulp.src('./wp-content/themes/orto-estetica/sass/**/*.scss')
    .pipe(sass({outputStyle: 'compressed'}).on('error',sass.logError))
    .pipe(gulp.dest('./wp-content/themes/orto-estetica/css'))
    .pipe(browserSync.stream());
});

// Roda o comando watch
gulp.watch('./wp-content/themes/orto-estetica/sass/**/*.scss', ['sass']);

gulp.task('watch', ['browserSync','sass','compress'], function(){
    gulp.watch('./wp-content/themes/orto-estetica/sass/**/*.scss', ['sass']);
    gulp.watch('./wp-content/themes/orto-estetica/*.php', browserSync.reload); 
    gulp.watch('./wp-content/themes/orto-estetica/js/**/*.js', browserSync.reload);
});

// Atualiza o browser
gulp.task('browserSync', function() {
  //watch files
    var files = [
    './wp-content/themes/orto-estetica/sass/**/*.scss'
    ];

  browserSync.init(files,{
      proxy: "localhost/orto-estetica/"
  }); 
});

// Minifica os JS
gulp.task('compress', function (callback) {
  pump([
      gulp.src('./wp-content/themes/orto-estetica/js/*.js'),
      uglify(),
      gulp.dest('./wp-content/themes/orto-estetica/js/min')
    ],
    callback
  );
});

// Realiza tarefas na sequência
gulp.task('default', function (callback) {
  runSequence(['sass','compress','browserSync', 'watch'],
    callback
  )
});